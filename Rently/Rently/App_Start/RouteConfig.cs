﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Rently
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            //routes.MapRoute(
            //    name: "CarsByProductionDate",
            //    url: "cars/produced/{year}/{month}",
            //    defaults: new { controller = "Cars", action = "ByProductionDate" },
            //    constraints: new { year = @"2015|2016", month = @"\d{2}" });


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
