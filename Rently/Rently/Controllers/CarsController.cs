﻿using System;
using Rently.Models;
using System.Web.Mvc;

namespace Rently.Controllers
{
    public class CarsController : Controller
    {
        // cars/random
        public ActionResult Random()
        {
            var car = new Car() { Name = "BMW" };

            ViewData.Model = car;


            return View();
        }

        // cars/edit/{id}
        public ActionResult Edit(int id)
        {
            return Content($"id={id}");
        }

        // cars?pageIndex=1&sortBy=Name
        public ActionResult Index(int? pageIndex, string sortBy)
        {
            pageIndex = pageIndex.HasValue ? pageIndex : 1;
            sortBy = !string.IsNullOrWhiteSpace(sortBy) ? sortBy : "Name";

            return Content($"pageIndex={pageIndex}&sortBy={sortBy}");
        }



        // cars/produced
        [Route("cars/produced/{year:min(2000)}/{month:regex(\\d{4}):range(1,12)}/{manufacturer:minlength(3)}")]
        public ActionResult ByProductionDate(int year, int month, string manufacturer = "audi")  
        {
            return Content($"{year}/{month}/{manufacturer}");
        }
    }
}